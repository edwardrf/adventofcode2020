package main

import (
	"bufio"
	"fmt"
	"io"
	"os"
)

type Ins struct {
	Op string
	Pa int
}

func main() {
	in := bufio.NewReader(os.Stdin)

	prog := make([]Ins, 0)
	for {
		line, err := in.ReadString('\n')
		if err == io.EOF {
			if line != "" {
				panic("None empty last line : " + line)
			}
			break
		}
		if err != nil {
			panic(err)
		}

		var ins Ins
		_, err = fmt.Sscanf(line, "%s %d", &ins.Op, &ins.Pa)

		prog = append(prog, ins)
	}

	for i, ins := range prog {
		if ins.Op == "jmp" {
			prog[i].Op = "nop"
			l, a := test(prog)
			if l {
				fmt.Println(i, l, a)
			}
			prog[i].Op = "jmp"
		}
		if ins.Op == "nop" {
			prog[i].Op = "jmp"
			l, a := test(prog)
			if l {
				fmt.Println(i, l, a)
			}
			prog[i].Op = "nop"
		}

	}
}

func test(prog []Ins) (bool, int) {
	acc := 0
	pc := 0
	ran := make(map[int]bool)
	for pc < len(prog) {
		ins := prog[pc]
		if ran[pc] {
			return false, acc
		}
		ran[pc] = true
		switch ins.Op {
		case "acc":
			acc += ins.Pa
		case "jmp":
			pc += ins.Pa
			continue
		case "nop":
		}
		pc++
	}
	return true, acc
}
