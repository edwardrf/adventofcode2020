package main

import (
	"bufio"
	"fmt"
	"io"
	"os"
)

type Ins struct {
	Op string
	Pa int
}

func main() {
	in := bufio.NewReader(os.Stdin)

	prog := make([]Ins, 0)
	for {
		line, err := in.ReadString('\n')
		if err == io.EOF {
			if line != "" {
				panic("None empty last line : " + line)
			}
			break
		}
		if err != nil {
			panic(err)
		}

		var ins Ins
		_, err = fmt.Sscanf(line, "%s %d", &ins.Op, &ins.Pa)

		prog = append(prog, ins)
	}

	acc := 0
	pc := 0
	ran := make(map[int]bool)
	for {
		ins := prog[pc]
		fmt.Println(ins)
		if ran[pc] {
			break
		}
		ran[pc] = true
		switch ins.Op {
		case "acc":
			acc += ins.Pa
		case "jmp":
			pc += ins.Pa
			continue
		case "nop":
		}
		pc++
	}

	fmt.Println(acc)
}
