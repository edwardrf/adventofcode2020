package main

import (
	"bufio"
	"fmt"
	"io"
	"os"
)

const N = 25

func main() {
	in := bufio.NewReader(os.Stdin)

	ns := make([]int, 0)
	for {
		var n int
		_, err := fmt.Fscanf(in, "%d\n", &n)
		if err == io.EOF {
			break
		}
		if err != nil {
			panic(err)
		}

		ns = append(ns, n)
	}

	for i := 0; i < len(ns)-N-1; i++ {
		if !check(ns[i:i+N], ns[i+N]) {
			fmt.Println(ns[i+N])
			break
		}
	}
}

func check(ns []int, last int) bool {
	m := make(map[int]bool)
	for _, n := range ns {
		if m[last-n] {
			return true
		}
		m[n] = true
	}
	return false
}
