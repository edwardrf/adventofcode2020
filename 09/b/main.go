package main

import (
	"bufio"
	"fmt"
	"io"
	"os"
)

const N = 25

func main() {
	in := bufio.NewReader(os.Stdin)

	ns := make([]int, 0)
	for {
		var n int
		_, err := fmt.Fscanf(in, "%d\n", &n)
		if err == io.EOF {
			break
		}
		if err != nil {
			panic(err)
		}

		ns = append(ns, n)
	}

	target := 0
	pos := 0
	for i := 0; i < len(ns)-N-1; i++ {
		if !check(ns[i:i+N], ns[i+N]) {
			fmt.Println(ns[i+N])
			target = ns[i+N]
			pos = i + N
		}
	}

	for i := 0; i < pos-1; i++ {

		for k := i + 1; k < pos; k++ {
			ls := 0
			for l := i; l < k; l++ {
				ls += ns[l]
			}
			if ls == target {
				min, max := ns[i], ns[i]
				for j := i; j < k; j++ {
					fmt.Println(ns[j])
					if ns[j] < min {
						min = ns[j]
					}
					if ns[j] > max {
						max = ns[j]
					}
				}
				fmt.Println(i, k, min+max)
			}
		}

	}
}

func check(ns []int, last int) bool {
	m := make(map[int]bool)
	for _, n := range ns {
		if m[last-n] {
			return true
		}
		m[n] = true
	}
	return false
}
