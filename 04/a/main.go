package main

import (
	"bufio"
	"fmt"
	"io"
	"os"
	"strings"
)

func main() {
	in := bufio.NewReader(os.Stdin)

	fields := map[string]int{
		"byr": 1,
		"iyr": 2,
		"eyr": 4,
		"hgt": 8,
		"hcl": 16,
		"ecl": 32,
		"pid": 64,
		//"cid": 128,
	}
	_ = fields

	cnt := 0
	done := false
	for !done {
		var entry string
		for {
			line, err := in.ReadString('\n')
			if err == io.EOF {
				done = true
				break
			}
			if err != nil {
				panic(err)
			}

			line = strings.TrimSpace(line)

			fmt.Printf("LINE [%s]\n", line)
			if line == "" {
				break
			}
			entry += " " + line
		}
		entry = strings.TrimSpace(entry)

		fmt.Printf("ENTRY: |%v|\n", entry)
		blks := strings.Split(entry, " ")
		fmt.Printf("BLKS: %v\n", blks)
		sum := 0
		for _, blk := range blks {
			fmt.Println(blk)
			pair := strings.Split(blk, ":")
			key, val := pair[0], pair[1]
			_ = val

			if v, ok := fields[key]; ok {
				sum += v
			}
		}
		if sum == 127 {
			cnt++
		}
	}

	fmt.Println(cnt)
}
