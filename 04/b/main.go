package main

import (
	"bufio"
	"fmt"
	"io"
	"os"
	"regexp"
	"strconv"
	"strings"
)

func main() {
	in := bufio.NewReader(os.Stdin)

	fields := map[string]int{
		"byr": 1,
		"iyr": 2,
		"eyr": 4,
		"hgt": 8,
		"hcl": 16,
		"ecl": 32,
		"pid": 64,
		//"cid": 128,
	}

	ecls := map[string]bool{"amb": true, "blu": true, "brn": true, "gry": true, "grn": true, "hzl": true, "oth": true}

	vp := map[string]func(string) bool{
		"byr": func(str string) bool {
			v, err := strconv.Atoi(str)
			if err != nil {
				return false
			}
			if v > 2002 || v < 1920 {
				return false
			}
			return true
		},
		"iyr": func(str string) bool {
			v, err := strconv.Atoi(str)
			if err != nil {
				return false
			}
			if v > 2020 || v < 2010 {
				return false
			}
			return true
		},
		"eyr": func(str string) bool {
			v, err := strconv.Atoi(str)
			if err != nil {
				return false
			}
			if v > 2030 || v < 2020 {
				return false
			}
			return true
		},
		"hgt": func(str string) bool {
			var val int
			var unit string

			_, err := fmt.Sscanf(str, "%d%s", &val, &unit)
			if err != nil {
				//fmt.Println(err, " ==> ", str, val, unit)
				return false
			}

			var min, max int
			if unit == "cm" {
				min, max = 150, 193
			} else if unit == "in" {
				min, max = 59, 76
			} else {
				//fmt.Printf("WRONG unit: '%v' from '%v'", unit, str)
				return false
			}
			if val > max || val < min {
				return false
			}
			return true
		},
		"hcl": regexp.MustCompile(`^#[0-9a-f]{6}$`).MatchString,
		"ecl": func(str string) bool { return ecls[str] },
		"pid": regexp.MustCompile(`^[0-9]{9}$`).MatchString,
		//"cid": regexp.MustCompile(`#[0-9a-f]{6}`).MatchString,
	}

	cnt := 0
	done := false
	for !done {
		var entry string
		for {
			line, err := in.ReadString('\n')
			if err == io.EOF {
				done = true
				break
			}
			if err != nil {
				panic(err)
			}

			line = strings.TrimSpace(line)

			if line == "" {
				break
			}
			entry += " " + line
		}
		entry = strings.TrimSpace(entry)

		blks := strings.Split(entry, " ")
		sum := 0
		for _, blk := range blks {
			pair := strings.Split(blk, ":")
			key, val := pair[0], pair[1]
			_ = val

			v, vk := fields[key]
			var vv bool
			if vf, ok := vp[key]; ok {
				vv = vf(val)
			}

			//if !vv && key == "ecl" {
			//fmt.Printf("Invalid %v: %v\n", key, val)
			//}
			//if vv && key == "pid" {
			//fmt.Printf("Valid %v: %v\n", key, val)
			//}

			if vk && vv {
				sum += v
			}
		}
		if sum == 127 {
			cnt++
		}
	}

	fmt.Println(cnt)
}
