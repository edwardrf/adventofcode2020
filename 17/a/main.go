package main

import (
	"bufio"
	"fmt"
	"io"
	"os"
)

type P struct {
	x, y, z int
}

func main() {
	in := bufio.NewReader(os.Stdin)

	mp := make(map[P]bool)
	done := false
	i := 0
	var min, max P
	for !done {
		line, err := in.ReadString('\n')
		if err == io.EOF {
			done = true
		} else if err != nil {
			panic(err)
		}
		if line == "" {
			break
		}

		for j, c := range line {
			if c == '#' {
				mp[P{j, i, 0}] = true
			}
		}

		i++
		max.x = len(line) - 1 // \n
		max.y = i
		max.z = 1
	}

	for i := 0; i < 6; i++ {
		mp, min, max = Step(mp, min, max)
	}

	Print(mp, min, max)
}

func Step(mp map[P]bool, min, max P) (map[P]bool, P, P) {
	nm := make(map[P]bool)
	for k := min.z - 1; k <= max.z; k++ {
		for j := min.y - 1; j <= max.y; j++ {
			for i := min.x - 1; i <= max.x; i++ {
				tp := P{i, j, k}
				if Test(mp, tp) {
					nm[tp] = true
					if tp.x < min.x {
						min.x = tp.x
					}
					if tp.y < min.y {
						min.y = tp.y
					}
					if tp.z < min.z {
						min.z = tp.z
					}
					if tp.x >= max.x {
						max.x = tp.x + 1
					}
					if tp.y >= max.y {
						max.y = tp.y + 1
					}
					if tp.z >= max.z {
						max.z = tp.z + 1
					}
				}
			}
		}
	}
	return nm, min, max
}

func Test(mp map[P]bool, p P) bool {
	an := 0
	for k := p.z - 1; k <= p.z+1; k++ {
		for j := p.y - 1; j <= p.y+1; j++ {
			for i := p.x - 1; i <= p.x+1; i++ {
				np := P{i, j, k}
				if np != p && mp[np] {
					an++
				}
			}
		}
	}
	if an == 3 {
		return true
	}
	if mp[p] && an == 2 {
		return true
	}
	return false
}

func Print(mp map[P]bool, min, max P) {
	cnt := 0
	for k := min.z; k < max.z; k++ {
		fmt.Printf("\nZ = %d\n", k)
		for j := min.y; j < max.y; j++ {
			for i := min.x; i < max.x; i++ {
				if mp[P{i, j, k}] {
					cnt++
					fmt.Printf("#")
				} else {
					fmt.Printf(".")
				}
			}
			fmt.Println()
		}
	}
	fmt.Printf("\nTotal: %d\n", cnt)
}
