package main

import (
	"bufio"
	"fmt"
	"io"
	"math"
	"os"
)

type Move struct {
	Action   byte
	Distance float64
}

type Point struct {
	x, y float64
}

func main() {
	in := bufio.NewReader(os.Stdin)

	ms := make([]Move, 0)
	for {
		var m Move
		_, err := fmt.Fscanf(in, "%c%f\n", &m.Action, &m.Distance)
		if err == io.EOF {
			break
		}
		if err != nil {
			panic(err)
		}

		ms = append(ms, m)
	}

	var s Point
	var wp = Point{10, 1}
	for _, m := range ms {
		switch m.Action {
		case 'N':
			wp.y += m.Distance
		case 'S':
			wp.y -= m.Distance
		case 'E':
			wp.x += m.Distance
		case 'W':
			wp.x -= m.Distance
		case 'R':
			m.Distance = -m.Distance
			fallthrough
		case 'L':
			a := m.Distance / 180 * math.Pi
			wp.x, wp.y = wp.x*math.Cos(a)-wp.y*math.Sin(a), wp.x*math.Sin(a)+wp.y*math.Cos(a)
		case 'F':
			s.x += wp.x * m.Distance
			s.y += wp.y * m.Distance
		}

		fmt.Printf("%v -- %v\n", wp, s)
	}

	fmt.Printf("{%v, %v}, %v\n", s.x, s.y, math.Abs(s.x)+math.Abs(s.y))
}
