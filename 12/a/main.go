package main

import (
	"bufio"
	"fmt"
	"io"
	"math"
	"os"
)

type Move struct {
	Action   byte
	Distance float64
}

type Ship struct {
	x, y, dir float64
}

func main() {
	in := bufio.NewReader(os.Stdin)

	ms := make([]Move, 0)
	for {
		var m Move
		_, err := fmt.Fscanf(in, "%c%f\n", &m.Action, &m.Distance)
		if err == io.EOF {
			break
		}
		if err != nil {
			panic(err)
		}

		ms = append(ms, m)
	}

	var s Ship
	for _, m := range ms {
		switch m.Action {
		case 'N':
			s.y += m.Distance
		case 'S':
			s.y -= m.Distance
		case 'E':
			s.x += m.Distance
		case 'W':
			s.x -= m.Distance
		case 'L':
			s.dir += m.Distance
		case 'R':
			s.dir -= m.Distance
		case 'F':
			dir := s.dir / 180 * math.Pi
			dx := math.Cos(dir) * m.Distance
			dy := math.Sin(dir) * m.Distance
			s.x += dx
			s.y += dy
		}
	}

	fmt.Printf("{%v, %v}, %v\n", s.x, s.y, math.Abs(s.x)+math.Abs(s.y))
}
