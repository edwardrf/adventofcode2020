package main

import (
	"fmt"
	"io"
	"sort"
)

func main() {
	seats := make([]int, 0)
	for {
		var str string
		_, err := fmt.Scanf("%s", &str)
		if err == io.EOF {
			break
		}
		var r, s int
		for _, c := range str {
			switch c {
			case 'B':
				r++
				fallthrough
			case 'F':
				r <<= 1
			case 'R':
				s++
				fallthrough
			case 'L':
				s <<= 1
			}
		}
		r >>= 1
		s >>= 1
		seats = append(seats, r*8+s)
	}
	sort.Ints(seats)

	for i, v := range seats {
		if i == 0 {
			continue
		}

		if v-seats[i-1] > 1 {
			fmt.Println(v, seats[i-1])
		}
	}
}
