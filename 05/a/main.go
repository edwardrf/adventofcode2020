package main

import (
	"fmt"
	"io"
)

func main() {
	max := 0
	for {
		var str string
		_, err := fmt.Scanf("%s", &str)
		if err == io.EOF {
			break
		}
		var r, s int
		for _, c := range str {
			switch c {
			case 'B':
				r++
				fallthrough
			case 'F':
				r <<= 1
			case 'R':
				s++
				fallthrough
			case 'L':
				s <<= 1
			}
		}
		r >>= 1
		s >>= 1
		//fmt.Println(r, s, r*8+s)
		id := r*8 + s
		if id > max {
			max = id
		}
	}
	fmt.Println(max)
}
