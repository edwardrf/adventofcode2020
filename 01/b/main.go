package main

import (
	"fmt"
	"io"
)

func main() {
	nm := make(map[int]bool)
	for {
		var n int
		_, err := fmt.Scanf("%d", &n)
		if err == io.EOF {
			break
		}
		if err != nil {
			panic(err)
		}
		nm[n] = true
	}

	for n, _ := range nm {
		t2 := 2020 - n
		for m, _ := range nm {
			if m != n && nm[t2-m] {
				fmt.Printf("%d\n", n*m*(t2-m))
			}
		}
	}
}
