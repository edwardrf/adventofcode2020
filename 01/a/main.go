package main

import "fmt"

func main() {
	nm := make(map[int]bool)
	for {
		var n int
		_, err := fmt.Scanf("%d", &n)
		if err != nil {
			panic(err)
		}
		if nm[2020-n] {
			fmt.Printf("%d\n", n*(2020-n))
			break
		}
		nm[n] = true
	}
}
