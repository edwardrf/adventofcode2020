package main

import (
	"fmt"
	"io"
)

func main() {
	m := make([]string, 0)

	for {
		var str string
		_, err := fmt.Scanf("%s", &str)
		if err == io.EOF {
			break
		}
		m = append(m, str)
	}

	fmt.Println(trees(m, 1, 1) * trees(m, 3, 1) * trees(m, 5, 1) * trees(m, 7, 1) * trees(m, 1, 2))
}

func trees(m []string, dx, dy int) int {
	cnt := 0
	for x, y := 0, 0; y < len(m); x, y = x+dx, y+dy {
		xx := x % len(m[y])
		if m[y][xx] == '#' {
			cnt++
		}
	}
	return cnt
}
