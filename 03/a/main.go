package main

import (
	"fmt"
	"io"
)

func main() {
	m := make([]string, 0)

	for {
		var str string
		_, err := fmt.Scanf("%s", &str)
		if err == io.EOF {
			break
		}
		m = append(m, str)
	}

	cnt := 0
	for x, y := 0, 0; y < len(m); x, y = x+3, y+1 {
		dx := x % len(m[y])
		if m[y][dx] == '#' {
			cnt++
		}
	}
	fmt.Println(cnt)
}
