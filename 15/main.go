package main

import (
	"fmt"
)

const N = 30000000

func main() {
	his := make(map[int]int)

	done := false
	var pos, ln int
	for pos = 0; !done; pos++ {
		var i int
		_, err := fmt.Scanf("%d,", &i)
		if err != nil {
			done = true
			ln = i
			break
		}
		his[i] = pos
	}

	for ; pos < N-1; pos++ {
		nn := 0
		if lp, ok := his[ln]; ok {
			nn = pos - lp
		}
		his[ln] = pos
		ln = nn
	}
	fmt.Println(ln)
}
