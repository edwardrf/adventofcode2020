package main

import (
	"fmt"
	"io"
)

func main() {
	valid := 0
	for {
		var p1, p2 int
		var chr byte
		var pswd string
		_, err := fmt.Scanf("%d-%d %c: %s", &p1, &p2, &chr, &pswd)
		if err == io.EOF {
			break
		}
		if err != nil {
			panic(err)
		}
		cnt := 0
		if pswd[p1-1] == chr {
			cnt++
		}
		if pswd[p2-1] == chr {
			cnt++
		}
		if cnt == 1 {
			valid++
		}
	}

	fmt.Printf("%d\n", valid)
}
