package main

import (
	"fmt"
	"io"
)

func main() {
	valid := 0
	for {
		var min, max int
		var chr rune
		var pswd string
		_, err := fmt.Scanf("%d-%d %c: %s", &min, &max, &chr, &pswd)
		if err == io.EOF {
			break
		}
		if err != nil {
			panic(err)
		}
		//fmt.Printf("%d ~ %d, [%c] -- %s\n", min, max, chr, pswd)
		cnt := 0
		for _, c := range pswd {
			if c == chr {
				cnt++
			}
		}
		if cnt >= min && cnt <= max {
			valid++
		}
	}

	fmt.Printf("%d\n", valid)
}
