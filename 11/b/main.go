package main

import (
	"bufio"
	"fmt"
	"io"
	"os"
)

func main() {
	in := bufio.NewReader(os.Stdin)

	m := make([][]byte, 0)
	for {
		l, err := in.ReadBytes('\n')
		if err == io.EOF {
			break
		}
		if err != nil {
			panic(err)
		}

		m = append(m, l)
	}

	cnt := 0
	for {
		for _, l := range m {
			fmt.Printf("%s", l)
		}
		fmt.Println()
		nm := step(m)
		//if cnt > 3 {
		//break
		//}
		cnt++
		if eq(m, nm) {
			for _, l := range m {
				fmt.Printf("%s", l)
			}
			fmt.Println()
			seated := 0
			for _, l := range m {
				for _, s := range l {
					if s == '#' {
						seated++
					}
				}
			}
			fmt.Println(seated)
			break
		}
		m = nm
	}
}

func eq(a, b [][]byte) bool {
	if len(a) != len(b) {
		panic("Map size different")
	}

	for i, l := range a {
		s := b[i]
		if len(l) != len(s) {
			panic("row size different")
		}
		for j, o := range l {
			t := s[j]
			if t != o {
				return false
			}
		}
	}
	return true
}

func step(m [][]byte) [][]byte {

	nm := make([][]byte, len(m))
	for i, l := range m {
		nr := make([]byte, len(l))
		nm[i] = nr
		for j, _ := range l {
			if m[i][j] == '.' {
				nm[i][j] = '.'
				continue
			}

			t := getns(m, i, j)
			if m[i][j] == 'L' && t == 0 {
				nm[i][j] = '#'
			} else if m[i][j] == '#' && t >= 5 {
				nm[i][j] = 'L'
			} else {
				nm[i][j] = m[i][j]
			}
		}
	}
	return nm
}

var dts = [][]int{
	[]int{-1, -1},
	[]int{-1, 0},
	[]int{-1, 1},
	[]int{0, -1},
	[]int{0, 1},
	[]int{1, -1},
	[]int{1, 0},
	[]int{1, 1},
}

func getns(m [][]byte, i, j int) int {
	t := 0
	for _, dt := range dts {
		li, lj := i, j
		for {
			li += dt[0]
			lj += dt[1]
			if li < 0 || lj < 0 || li >= len(m) || lj >= len(m[0]) {
				break
			}
			if m[li][lj] == 'L' {
				break
			}
			if m[li][lj] == '#' {
				t++
				break
			}
		}
	}

	return t
}
