package main

import (
	"bufio"
	"fmt"
	"io"
	"os"
)

func main() {
	in := bufio.NewReader(os.Stdin)

	m := make([][]byte, 0)
	for {
		l, err := in.ReadBytes('\n')
		if err == io.EOF {
			break
		}
		if err != nil {
			panic(err)
		}

		m = append(m, l)
	}

	for {
		for _, l := range m {
			fmt.Printf("%s\n", l)
		}
		fmt.Println()
		nm := step(m)
		if eq(m, nm) {
			seated := 0
			for _, l := range m {
				for _, s := range l {
					if s == '#' {
						seated++
					}
				}
			}
			fmt.Println(seated)
			break
		}
		m = nm
	}
}

func eq(a, b [][]byte) bool {
	if len(a) != len(b) {
		panic("Map size different")
	}

	for i, l := range a {
		s := b[i]
		if len(l) != len(s) {
			panic("row size different")
		}
		for j, o := range l {
			t := s[j]
			if t != o {
				return false
			}
		}
	}
	return true
}

func step(m [][]byte) [][]byte {

	nm := make([][]byte, len(m))
	for i, l := range m {
		nr := make([]byte, len(l))
		nm[i] = nr
		for j, _ := range l {
			if m[i][j] == '.' {
				nm[i][j] = '.'
				continue
			}

			t := getns(m, i, j)
			if m[i][j] == 'L' && t == 0 {
				nm[i][j] = '#'
			} else if m[i][j] == '#' && t >= 4 {
				nm[i][j] = 'L'
			} else {
				nm[i][j] = m[i][j]
			}
		}
	}
	return nm
}

func getns(m [][]byte, i, j int) int {
	t := 0
	if i-1 >= 0 {
		if j-1 >= 0 {
			if m[i-1][j-1] == '#' {
				t++
			}
		}
		if j+1 < len(m[i]) {
			if m[i-1][j+1] == '#' {
				t++
			}
		}
		if m[i-1][j] == '#' {
			t++
		}
	}

	if i+1 < len(m) {
		if j-1 >= 0 {
			if m[i+1][j-1] == '#' {
				t++
			}
		}
		if j+1 < len(m[i]) {
			if m[i+1][j+1] == '#' {
				t++
			}
		}
		if m[i+1][j] == '#' {
			t++
		}
	}

	if j-1 >= 0 {
		if m[i][j-1] == '#' {
			t++
		}
	}
	if j+1 < len(m[i]) {
		if m[i][j+1] == '#' {
			t++
		}
	}

	return t

}
