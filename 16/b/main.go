package main

import (
	"bufio"
	"fmt"
	"io"
	"os"
	"strconv"
	"strings"
)

type Range struct {
	min, max int
}

type Rule struct {
	Name   string
	Ranges []Range
}

func (rule Rule) match(v int) bool {
	valid := false
	for _, rge := range rule.Ranges {
		if v >= rge.min && v <= rge.max {
			valid = true
		}
	}
	return valid
}

func main() {
	in := bufio.NewReader(os.Stdin)
	rules := []Rule{}
	for {
		line, err := in.ReadString('\n')
		line = strings.TrimSpace(line)
		if line == "" {
			break
		}
		if err != nil {
			panic(err)
		}
		r := Rule{"", []Range{Range{}, Range{}}}
		parts := strings.Split(line, ":")
		r.Name = parts[0]
		_, err = fmt.Sscanf(parts[1], "%d-%d or %d-%d", &r.Ranges[0].min, &r.Ranges[0].max, &r.Ranges[1].min, &r.Ranges[1].max)
		if err != nil {
			panic(err)
		}
		rules = append(rules, r)
	}

	yts := ""
	_, err := fmt.Fscanf(in, "your ticket:\n%s\n\nnearby tickets:\n", &yts)
	if err != nil {
		panic(err)
	}
	yt := parseTicket(yts)

	nts := [][]int{}
	done := false
	for !done {
		line, err := in.ReadString('\n')
		if err == io.EOF {
			done = true
		} else if err != nil {
			panic(err)
		}
		line = strings.TrimSpace(line)
		if line == "" {
			continue
		}
		nts = append(nts, parseTicket(line))
	}

	vts := [][]int{}
	for _, nt := range nts {
		vt := true
	ticketLoop:
		for _, vf := range nt {
			for _, rule := range rules {
				if rule.match(vf) {
					continue ticketLoop
				}
			}
			vt = false
			break
		}
		if vt {
			vts = append(vts, nt)
		}
	}

	//fmt.Println(vts)
	rm := make(map[int]map[string]Rule)
	for i := 0; i < len(yt); i++ {
		vrs := make(map[string]Rule)
	ruleLoop:
		for _, rule := range rules {
			for _, vt := range vts {
				fv := vt[i]
				if !rule.match(fv) {
					continue ruleLoop
				}
			}
			vrs[rule.Name] = rule
		}
		rm[i] = vrs
	}
	for i, r := range rm {
		fmt.Printf("%3d: %v\n", i, r)
	}

	for {
		removed := 0
		for i, r := range rm {
			if len(r) == 1 {
				name := ""
				for _, r0 := range r {
					name = r0.Name
				}
				for j := 0; j < len(rm); j++ {
					if j == i {
						continue
					}
					if _, ok := rm[j][name]; ok {
						delete(rm[j], name)
						fmt.Printf("Removing %s from %d\n", name, j)
						removed++
					}
				}
			}
		}
		if removed == 0 {
			break
		}
	}

	fmt.Println("\nAFTER")
	res := 1
	for i, r := range rm {
		name := ""
		for _, r0 := range r {
			name = r0.Name
		}
		if strings.HasPrefix(name, "departure") {
			res *= yt[i]
		}
		fmt.Printf("%3d: %3d => %v\n", i, len(r), r)
	}

	fmt.Println(res)
}

func parseTicket(str string) []int {
	ytps := strings.Split(str, ",")
	yt := []int{}
	for _, ts := range ytps {
		v, err := strconv.Atoi(ts)
		if err != nil {
			panic(err)
		}
		yt = append(yt, v)
	}
	return yt
}
