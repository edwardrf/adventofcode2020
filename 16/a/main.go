package main

import (
	"bufio"
	"fmt"
	"io"
	"os"
	"strconv"
	"strings"
)

type Range struct {
	min, max int
}

type Rule struct {
	Name   string
	Ranges []Range
}

func main() {
	in := bufio.NewReader(os.Stdin)
	rules := []Rule{}
	for {
		line, err := in.ReadString('\n')
		line = strings.TrimSpace(line)
		if line == "" {
			break
		}
		if err != nil {
			panic(err)
		}
		r := Rule{"", []Range{Range{}, Range{}}}
		parts := strings.Split(line, ":")
		r.Name = parts[0]
		_, err = fmt.Sscanf(parts[1], "%d-%d or %d-%d", &r.Ranges[0].min, &r.Ranges[0].max, &r.Ranges[1].min, &r.Ranges[1].max)
		if err != nil {
			panic(err)
		}
		rules = append(rules, r)
	}

	yts := ""
	_, err := fmt.Fscanf(in, "your ticket:\n%s\n\nnearby tickets:\n", &yts)
	if err != nil {
		panic(err)
	}
	yt := parseTicket(yts)
	_ = yt

	nts := [][]int{}
	done := false
	for !done {
		line, err := in.ReadString('\n')
		if err == io.EOF {
			done = true
		} else if err != nil {
			panic(err)
		}
		line = strings.TrimSpace(line)
		if line == "" {
			continue
		}
		nts = append(nts, parseTicket(line))
	}

	//fmt.Println(rules)
	//fmt.Println(yt)
	//fmt.Println(nts)

	sum := 0
	for _, nt := range nts {
	ticketLoop:
		for _, vf := range nt {
			for _, rule := range rules {
				valid := false
				for _, rge := range rule.Ranges {
					if vf >= rge.min && vf <= rge.max {
						valid = true
					}
				}
				if valid {
					fmt.Printf("%v is valid for %v\n", vf, rule)
					continue ticketLoop
				}
			}
			sum += vf
		}
	}
	fmt.Println(sum)
}

func parseTicket(str string) []int {
	ytps := strings.Split(str, ",")
	yt := []int{}
	for _, ts := range ytps {
		v, err := strconv.Atoi(ts)
		if err != nil {
			panic(err)
		}
		yt = append(yt, v)
	}
	return yt
}
