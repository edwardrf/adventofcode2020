package main

import (
	"fmt"
	"io"
	"strconv"
	"strings"
)

func main() {
	done := false
	mem := make(map[int]uint64)

	var m0, m1 uint64
	for !done {
		var cmd, val string
		_, err := fmt.Scanf("%s = %s\n", &cmd, &val)
		if err == io.EOF {
			done = true
		} else if err != nil {
			panic(err)
		}

		if cmd == "mask" {
			m0, m1 = 0, 0
			for i, c := range val {
				d := len(val) - i - 1
				if c == '1' {
					m1 |= 1 << d
				}
				if c == '0' {
					m0 |= 1 << d
				}
			}
			m0 = ^m0
			fmt.Printf("M0: [%b]\tM1:[%b]\n", m0, m1)
		} else if strings.TrimSpace(cmd) != "" {
			var addr int
			_, err := fmt.Sscanf(cmd, "mem[%d]", &addr)
			if err != nil {
				fmt.Println("CMD: ", cmd)
				panic(err)
			}
			val, err := strconv.ParseUint(val, 10, 64)
			if err != nil {
				panic(err)
			}

			val |= m1
			val &= m0

			mem[addr] = val
		}

	}
	var sum uint64
	for _, v := range mem {
		sum += v
	}
	fmt.Println(mem, sum)
}
