package main

import (
	"fmt"
	"io"
	"math/bits"
	"strconv"
	"strings"
)

func main() {
	done := false
	mem := make(map[uint64]uint64)

	var mask, mplx uint64
	for !done {
		var cmd, val string
		_, err := fmt.Scanf("%s = %s\n", &cmd, &val)
		if err == io.EOF {
			done = true
		} else if err != nil {
			panic(err)
		}

		if cmd == "mask" {
			mask = 0
			mplx = 0
			for i, c := range val {
				d := len(val) - i - 1
				if c == '1' {
					mask |= 1 << d
				}
				if c == 'X' {
					mplx |= 1 << d
				}
			}
			fmt.Printf("Mask: [%b]\tMplx:[%b]\n", mask, mplx)
		} else if strings.TrimSpace(cmd) != "" {
			var addr uint64
			_, err := fmt.Sscanf(cmd, "mem[%d]", &addr)
			if err != nil {
				fmt.Println("CMD: ", cmd)
				panic(err)
			}
			val, err := strconv.ParseUint(val, 10, 64)
			if err != nil {
				panic(err)
			}

			set(mem, addr|mask, val, mplx)
		}

	}
	var sum uint64
	for _, v := range mem {
		sum += v
	}
	fmt.Println(mem, sum)
}

func set(mem map[uint64]uint64, addr, val, mplx uint64) {
	if mplx == 0 {
		mem[addr] = val
		return
	}
	i := bits.TrailingZeros(uint(mplx))
	var pm uint64 = 1 << i
	var nmplx uint64 = mplx ^ pm
	set(mem, addr|pm, val, nmplx)
	set(mem, addr&(^pm), val, nmplx)
}
