package main

import "fmt"

func main() {
	a, ra, b, rb := 59, 55, 589, 335 //31, 25, 19, 12
	for i := 0; ; i++ {
		if i%a == ra && i%b == rb {
			fmt.Println(i)
			break
		}
	}
}
