package main

import (
	"bufio"
	"fmt"
	"io"
	"os"
	"strconv"
	"strings"
)

func main() {
	in := bufio.NewReader(os.Stdin)

	var start int
	_, err := fmt.Fscanf(in, "%d\n", &start)
	if err != nil {
		panic(err)
	}
	_ = start
	bus := make([][]int, 0)

	eof := false
	for i := 0; !eof; i++ {
		bs, err := in.ReadString(',')
		if err == io.EOF {
			eof = true
		} else if err != nil {
			panic(err)
		}
		bs = strings.Trim(bs, ",")
		bs = strings.TrimSpace(bs)
		if bs == "x" {
			continue
		}
		b, err := strconv.Atoi(bs)
		if err != nil {
			panic(err)
		}

		r := b - i
		for r < 0 {
			r += b
		}
		if r == b {
			r = 0
		}
		bus = append(bus, []int{b, r})
	}

	fmt.Printf("%v\n", bus)
	res := fb(bus)
	fmt.Printf("%d, %d\n", res, res[0]+res[1])
	//fmt.Printf("%d wait %d, ans: %d\n", mb, min, mb*min)
}

func fb(bus [][]int) []int {
	l := len(bus)
	if l == 1 {
		return bus[0]
	} else if l == 2 {
		fmt.Printf("%v <-> %v", bus[0], bus[1])
		var i int
		for i = 0; (i+bus[0][1])%bus[1][0] != bus[1][1]; i += bus[0][0] {
		}
		r := []int{bus[0][0] * bus[1][0], i + bus[0][1]}
		fmt.Printf(" ==> %v\n", r)
		return r
	}
	mid := len(bus) / 2
	return fb([][]int{fb(bus[0:mid]), fb(bus[mid:])})
}
