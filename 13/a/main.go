package main

import (
	"bufio"
	"fmt"
	"io"
	"os"
	"strconv"
	"strings"
)

func main() {
	in := bufio.NewReader(os.Stdin)

	var start int
	_, err := fmt.Fscanf(in, "%d\n", &start)
	if err != nil {
		panic(err)
	}
	bus := make([]int, 0)
	for {
		bs, err := in.ReadString(',')
		if err == io.EOF {
			break
		}
		if err != nil {
			panic(err)
		}
		bs = strings.Trim(bs, ",")
		if bs == "x" {
			continue
		}
		b, err := strconv.Atoi(bs)
		if err != nil {
			panic(err)
		}

		bus = append(bus, b)
	}

	fmt.Printf("%d\n%v\n", start, bus)
	min := bus[0]
	mb := 0
	for _, b := range bus {
		w := b - (start % b)
		if w < min {
			min = w
			mb = b
		}
	}
	fmt.Printf("%d wait %d, ans: %d\n", mb, min, mb*min)
}
