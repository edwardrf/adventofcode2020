package main

import (
	"bufio"
	"fmt"
	"io"
	"os"
)

func main() {
	in := bufio.NewReader(os.Stdin)

	ns := make([]int, 0)
	for {
		var n int
		_, err := fmt.Fscanf(in, "%d\n", &n)
		if err == io.EOF {
			break
		}
		if err != nil {
			panic(err)
		}

		ns = append(ns, n)
	}

	max := 0
	nm := make(map[int]bool)
	for _, n := range ns {
		if n > max {
			max = n
		}
		nm[n] = true
	}

	fmt.Println(check(nm, max))
}

var found = make(map[int]int)

func check(nm map[int]bool, t int) int {
	if f, ok := found[t]; ok {
		return f
	}
	sum := 0
	for i := 1; i <= 3; i++ {
		p := t - i
		if p == 0 {
			sum++
		}
		if nm[p] {
			sum += check(nm, p)
		}
	}
	found[t] = sum
	return sum
}
