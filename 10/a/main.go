package main

import (
	"bufio"
	"fmt"
	"io"
	"os"
)

func main() {
	in := bufio.NewReader(os.Stdin)

	ns := make([]int, 0)
	for {
		var n int
		_, err := fmt.Fscanf(in, "%d\n", &n)
		if err == io.EOF {
			break
		}
		if err != nil {
			panic(err)
		}

		ns = append(ns, n)
	}

	nm := make(map[int]bool)
	for _, n := range ns {
		nm[n] = true
	}

	check(nm, 0)
}

var moves = make([]int, 3)

func check(nm map[int]bool, curr int) {
	if len(nm) == 0 {
		fmt.Printf("%v, %d\n", moves, moves[0]*(moves[2]+1))
		return
	}
	for i := 0; i < 3; i++ {
		p := curr + i + 1
		if nm[p] {
			moves[i]++
			delete(nm, p)
			check(nm, p)
			//nm[p] = true
		}
	}
}
