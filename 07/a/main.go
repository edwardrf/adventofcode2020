package main

import (
	"bufio"
	"fmt"
	"io"
	"os"
	"strconv"
	"strings"
)

type tp struct {
	Name  string
	Count int
}

func main() {
	in := bufio.NewReader(os.Stdin)

	m := make(map[string][]tp)
	for {
		line, err := in.ReadString('\n')
		if err == io.EOF {
			if line != "" {
				panic("None empty last line : " + line)
			}
			break
		}
		if err != nil {
			panic(err)
		}

		lr := strings.NewReader(line)
		var adj, col string
		_, err = fmt.Fscanf(lr, "%s %s bags contain ", &adj, &col)
		if err != nil {
			fmt.Println(line)
			panic("Sscanf contains " + err.Error())
		}

		var tps []tp
		var bags string
		for {
			var lcnt int
			var lcs, ladj, lcol string
			_, err := fmt.Fscanf(lr, "%s", &lcs)
			if err != nil {
				fmt.Println(line)
				panic("Sscanf cnt " + err.Error())
			}
			if lcs == "no" {
				break
			}
			lcnt, err = strconv.Atoi(lcs)
			if err != nil {
				panic("invalid cnt " + lcs)
			}

			_, err = fmt.Fscanf(lr, "%s %s %s", &ladj, &lcol, &bags)
			if err != nil {
				fmt.Println(line)
				panic("Sscanf rest " + err.Error())
			}

			//fmt.Println(line)
			tps = append(tps, tp{ladj + " " + lcol, lcnt})
			if bags[len(bags)-1] == '.' {
				break
			}
		}

		m[adj+" "+col] = tps
	}

	rm := make(map[string][]string)
	for col, tps := range m {
		for _, tp := range tps {
			rm[tp.Name] = append(rm[tp.Name], col)
		}
	}

	stk := []string{"shiny gold"}
	cm := make(map[string]bool)
	for len(stk) > 0 {
		col := stk[0]
		stk = stk[1:]
		for _, pc := range rm[col] {
			stk = append(stk, pc)
			cm[pc] = true
		}
	}
	fmt.Println(rm)
	fmt.Println(len(cm))
}
