package main

import (
	"bufio"
	"fmt"
	"io"
	"os"
	"strings"
)

func main() {
	in := bufio.NewReader(os.Stdin)

	done := false
	ms := make([]map[rune]int, 0)
	sz := make([]int, 0)
	for !done {
		m := make(map[rune]int)
		l := 0
		for !done {
			line, err := in.ReadString('\n')
			if err != nil && err != io.EOF {
				panic(err)
			}
			if err == io.EOF {
				done = true
			}

			line = strings.TrimSpace(line)
			if line == "" {
				ms = append(ms, m)
				sz = append(sz, l)
				fmt.Println(l, m)
				m = make(map[rune]int)
				l = -1
			}

			for _, chr := range line {
				m[chr]++
			}
			l++
		}
	}

	sum := 0
	for i, m := range ms {
		for _, v := range m {
			if v == sz[i] {
				sum++
			}
		}
	}

	fmt.Println(sum)
}
