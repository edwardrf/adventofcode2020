package main

import (
	"bufio"
	"fmt"
	"io"
	"os"
	"strings"
)

func main() {
	in := bufio.NewReader(os.Stdin)

	done := false
	ms := make([]map[rune]bool, 0)
	for !done {
		m := make(map[rune]bool)
		for !done {
			line, err := in.ReadString('\n')
			if err != nil && err != io.EOF {
				panic(err)
			}
			if err == io.EOF {
				done = true
			}

			line = strings.TrimSpace(line)
			if line == "" {
				ms = append(ms, m)
				m = make(map[rune]bool)
			}

			for _, chr := range line {
				m[chr] = true
			}
		}
	}

	sum := 0
	for _, m := range ms {
		sum += len(m)
	}

	fmt.Println(sum)
}
